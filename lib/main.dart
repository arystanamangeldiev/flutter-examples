import 'package:flutter/material.dart';
import 'package:flutterexamples/common/constants/colors.dart';
import 'package:flutterexamples/common/constants/strings.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: welcomeTitle,
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(welcomeTitle),
        ),
      ),
    );
  }
}
